var pool = require('./databaseConfig.js');
var db_sikolamp = {
    getUser: function (username, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM t_user WHERE username = ?';
                conn.query(sql, [username], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                })

            }
        })
    },
    getUser1: function (username, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT hak_akses FROM t_user WHERE username = ?';
                conn.query(sql, [username], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    getLampu: function (username, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM t_lampu WHERE username = ?';
                conn.query(sql, [username], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    addLampu: function (id_lampu, status_lampu, status_fitur, username, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'INSERT INTO t_lampu (id_lampu, status_lampu, status_fitur, username) values (NULL,?,?,?)';
                conn.query(sql, [status_lampu, status_fitur, username], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    addUser: function (username, password, hak_akses, callback) { 
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'INSERT INTO t_user (username, password, hak_akses) values (?,?,?)';
                conn.query(sql, [username, password, hak_akses], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });

            }
        });
    },
    updateLampu: function (status_lampu,id_lampu,  callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                console.log(id_lampu+", "+status_lampu)
                var sql = 'UPDATE t_lampu SET status_lampu=? WHERE id_lampu=?';
                conn.query(sql, [status_lampu, id_lampu], function (err,result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });

            }
        });
    },
    updateFitur: function (status_fitur, id_lampu, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                console.log(id_lampu+", "+status_fitur)
                var sql = 'UPDATE t_lampu SET status_fitur =? WHERE id_lampu=?';
                conn.query(sql, [status_fitur, id_lampu], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    deleteLampu: function (id_lampu, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'DELETE FROM t_lampu WHERE id_lampu=?';
                conn.query(sql, [id_lampu], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    deleteUser: function (username, callback) {
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected!");
                var sql = 'DELETE FROM t_user WHERE username=?';
                conn.query(sql, [username], function (err, result) {
                    conn.release();
                    if (err) {
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
};
module.exports = db_sikolamp